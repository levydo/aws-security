# create secret and encrypt with kms key
resource "aws_secretsmanager_secret" "techtalk-secret" {
  name       = var.secret-name
  kms_key_id = aws_kms_key.sm-key.arn
}
# generate a random password
resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}
# add secret value
resource "aws_secretsmanager_secret_version" "secret-value" {
  secret_id     = aws_secretsmanager_secret.techtalk-secret.id
  secret_string = <<EOF
   {
    "username": "techtalk",
    "password": "${random_password.password.result}"
   }
EOF
}
