# Alias KMS
variable "sm-key-alias" {
  type        = string
  description = "Secret manager key alias"
  default     = "alias/techtalk-test-key"
}
# KMS username
variable "kms-username" {
  type        = string
  description = "KMS username"
  default     = "techtalk"
}
# Secret name
variable "secret-name" {
  type        = string
  description = "Secret name"
  default     = "techtalk-test-secret"
 
}