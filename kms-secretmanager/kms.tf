#KMS key  creation
resource "aws_kms_key" "sm-key" {
  description = "KMS key for Secret Manager"
  key_usage   = "ENCRYPT_DECRYPT"
  policy = templatefile("policies/kms-policy.json", {
    ACCOUNT_ID       = data.aws_caller_identity.current.account_id
    USER_NAME        = var.kms-username
  })
  enable_key_rotation     = true
  deletion_window_in_days = 7
}

# key alias
resource "aws_kms_alias" "sm-key-alias" {
  name          = var.sm-key-alias
  target_key_id = aws_kms_key.sm-key.key_id
}