terraform {
  backend "s3" {
    bucket         = "techtalk-tfstate"
    key            = "security-tfstate"
    region         = "eu-west-1"
    encrypt        = true
  }
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }

}